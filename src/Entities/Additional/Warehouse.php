<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Additional;

use Flax\Contracts\BeEntity;
use Flax\Entities\Additional\Additional\Depart;

/**
 * @property-read string $uuid
 * @property-read string $code
 * @property-read string $descr
 * @property-read string $address
 * @property-read array $Depart
 * */
class Warehouse extends FieldsEntity implements BeEntity
{
    public function depart(): Depart
    {
        return new Depart($this->Depart);
    }
}
