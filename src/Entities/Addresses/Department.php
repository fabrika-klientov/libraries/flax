<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Addresses;

use Flax\Contracts\BeEntity;
use Flax\Entities\Addresses\Additional\AreaSimple;
use Flax\Entities\Addresses\Additional\BranchTypeSimple;
use Flax\Entities\Addresses\Additional\CityRegionSimple;
use Flax\Entities\Addresses\Additional\CitySimple;
use Flax\Entities\Addresses\Additional\DepartmentSimple;
use Flax\Entities\Addresses\Additional\DepartTypeSimple;
use Flax\Entities\Addresses\Additional\DirectionSimple;
use Flax\Entities\Addresses\Additional\KnotSimple;
use Flax\Entities\Addresses\Additional\ModelSimple;
use Flax\Entities\Addresses\Additional\RegionSimple;
use Flax\Entities\Addresses\Additional\Schedule;
use Flax\Entities\Addresses\Additional\StreetSimple;

/**
 * @property-read string $code
 * @property-read string $descr
 * @property-read string $branch
 * @property-read array $Depart
 * @property-read array $objectOwner
 * @property-read array $region
 * @property-read string $regionSCOATOU
 * @property-read array $city
 * @property-read string $citySCOATOU
 * @property-read array $locality
 * @property-read string $localitySCOATOU
 * @property-read string $identificator
 * @property-read array $direction
 * @property-read string $idSheduler
 * @property-read string $codeHolding
 * @property-read array $knot
 * @property-read string $quereVisit
 * @property-read bool $agent
 * @property-read bool $possibility_to_pay_by_card
 * @property-read bool $possibility_to_accept_payment
 * @property-read int $weight_limit
 * @property-read bool $availability_of_parcel_locker
 * @property-read string $address
 * @property-read string $lat
 * @property-read string $lng
 * @property-read string $departNumber
 * @property-read array $TypeDepart
 * @property-read array $areaRegion
 * @property-read string $areaRegionSCOATOU
 * @property-read array $street
 * @property-read string $houseNumber
 * @property-read array $branchType
 * @property-read int $StatusDepart
 * @property-read bool $parcels_without_pay
 *
 * @property-read array $Schedule
 * */
class Department extends FieldsEntity implements BeEntity
{
    public function depart(): DepartmentSimple
    {
        return new DepartmentSimple($this->Depart);
    }

    public function objectOwner(): ModelSimple
    {
        return new ModelSimple($this->objectOwner);
    }

    public function region(): AreaSimple
    {
        return new AreaSimple($this->region);
    }

    public function city(): CitySimple
    {
        return new CitySimple($this->city);
    }

    public function locality(): CityRegionSimple
    {
        return new CityRegionSimple($this->locality);
    }

    public function direction(): DirectionSimple
    {
        return new DirectionSimple($this->direction);
    }

    public function knot(): KnotSimple
    {
        return new KnotSimple($this->knot);
    }

    public function typeDepart(): DepartTypeSimple
    {
        return new DepartTypeSimple($this->TypeDepart);
    }

    public function areaRegion(): RegionSimple
    {
        return new RegionSimple($this->areaRegion);
    }

    public function street(): StreetSimple
    {
        return new StreetSimple($this->street);
    }

    public function branchType(): BranchTypeSimple
    {
        return new BranchTypeSimple($this->branchType);
    }

    public function schedule(): Schedule
    {
        return new Schedule($this->Schedule);
    }

    public function __get($name)
    {
        switch ($name) {
            case 'Schedule':
                return $this->data[$name] ?? null;
            default:
                return parent::__get($name);
        }
    }
}
