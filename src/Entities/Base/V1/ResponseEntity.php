<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Base\V1;

use Flax\Entities\Entity;
use Flax\Exceptions\FlaxException;
use Illuminate\Support\Collection;

/**
 * @property-read string $result
 * @property-read array $errors
 * @property-read array $data
 * */
abstract class ResponseEntity extends Entity
{
    /**
     * @return ResponseError[]
     */
    public function errors(): array
    {
        return array_map(function ($item) {
            return new ResponseError($item);
        }, $this->errors ?? []);
    }

    /**
     * @return mixed
     */
    abstract public function data();

    /**
     * @throws FlaxException
     */
    protected function factoryObjectWithThrow(string $className)
    {
        return self::_factoryObjectWithThrow($this->getData(), $className);
    }

    /**
     * @throws FlaxException
     */
    protected function factoryObjectMainWithThrow(string $className)
    {
        return self::_factoryObjectWithThrow($this->data, $className);
    }

    /**
     * @throws FlaxException
     */
    private function _factoryObjectWithThrow(array $data, string $className)
    {
        if (!class_exists($className)) {
            throw new FlaxException("Class name [$className] is not resolved");
        }

        if (empty($data)) {
            throw new FlaxException('Data were returned as empty');
        }

        return new $className($data);
    }

    /**
     * @param string $className
     * @param bool $isCollection
     * @return array|Collection
     *
     * @throws FlaxException
     */
    protected function getCollectOfData(string $className, bool $isCollection = false)
    {
        return self::_getCollectOf($this->getData(), $className, $isCollection);
    }

    /**
     * @param string $className
     * @param bool $isCollection
     * @return array|Collection
     *
     * @throws FlaxException
     */
    protected function getCollectOfMain(string $className, bool $isCollection = false)
    {
        return self::_getCollectOf($this->data, $className, $isCollection);
    }

    /**
     * @throws FlaxException
     */
    private function _getCollectOf(array $data, string $className, bool $isCollection = false)
    {
        if (!class_exists($className)) {
            throw new FlaxException("Class name [$className] is not resolved");
        }

        $list = array_map(function ($item) use ($className) {
            return new $className($item);
        }, $data);

        if ($isCollection) {
            return new Collection($list);
        }

        return $list;
    }

    /** Via magic doesn't correct (inner the class)
     */
    protected function getData()
    {
        return $this->data['data'] ?? [];
    }
}
