<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Statuses\Responses;

use Flax\Contracts\BeResponseEntity;
use Flax\Entities\Statuses\StatusHistory;
use Illuminate\Support\Collection;

class ResponseStatusesHistory extends BaseResponse implements BeResponseEntity
{
    /**
     * @return StatusHistory[]
     */
    public function data(): array
    {
        return self::getCollectOfData(StatusHistory::class);
    }

    public function dataCollect(): Collection
    {
        return self::getCollectOfData(StatusHistory::class, true);
    }

    public function isEmpty(): bool
    {
        return $this->dataCollect()->isEmpty();
    }
}
