<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Addresses\Requests;

use Flax\Contracts\BeRequestEntity;

/**
 * @property int $filial_type
 *
 * @method self uuid($value, $rightValue = null, string $comparison = self::DEF_COMPARISON)
 * @method self descr($value, $rightValue = null, string $comparison = self::DEF_COMPARISON)
 * @method self region($value, $rightValue = null, string $comparison = self::DEF_COMPARISON)
 * @method self city($value, $rightValue = null, string $comparison = self::DEF_COMPARISON)
 * @method self StatusDepart($value, $rightValue = null, string $comparison = self::DEF_COMPARISON)
 * @method self branch($value, $rightValue = null, string $comparison = self::DEF_COMPARISON)
 * */
class DepartmentsRequest extends BaseRequest implements BeRequestEntity
{
    public const FILIAL_TYPE_ALL = 0;
    public const FILIAL_TYPE_FREE = 1;
    public const FILIAL_TYPE_ALL_DEPART = 2;

    public const STATUS_DEPART_ACTIVE = 1;
    public const STATUS_DEPART_TO_CLOSE = 2;
}
