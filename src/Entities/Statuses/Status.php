<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Statuses;

use Flax\Contracts\BeEntity;

/**
 * @property-read string $uuid
 * @property-read string $code
 * @property-read string $descr
 * @property-read bool $final
 * */
class Status extends FieldsEntity implements BeEntity
{

}
