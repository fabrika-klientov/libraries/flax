<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Core\Builder;

use Flax\Core\Classes\ActiveRecord;

class ParamsBuilder extends ActiveRecord
{
    public function set(string $key, $value)
    {
        $this->data[$key] = $value;

        return $this;
    }

    public function jsonSerialize()
    {
        if (empty($this->data)) {
            return null;
        }

        return parent::jsonSerialize();
    }
}
