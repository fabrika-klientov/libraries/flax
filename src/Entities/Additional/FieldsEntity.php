<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Additional;

use Flax\Entities\Entity as Base;

/**
 * @property-read array $fields
 * @property-read array $listDataFields
 * */
abstract class FieldsEntity extends Base
{
    public function __get($name)
    {
        switch ($name) {
            case 'fields':
            case 'listDataFields':
                return parent::__get($name);
            default:
                return $this->data['fields'][$name] ?? null;
        }
    }
}
