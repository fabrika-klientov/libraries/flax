<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Documents\Requests;

use Flax\Contracts\BeRequestEntity;

/**
 * @method self number(string $value)
 * @method self date(string $value)
 * @method self cargoType(string $value)
 * @method self cargoDetails(CargoDetail[] $value)
 * @method self senderCityId(string $value)
 * @method self senderCompany(string $value)
 * @method self senderContact(string $value)
 * @method self senderPhone(string $value)
 * @method self senderPickUpAddress(string $value)
 * @method self senderPickupPoint(string $value)
 * @method self pickUpIsRequired(bool $value)
 * @method self senderBranch(string $value)
 * @method self receiverCod(string $value)
 * @method self receiver(string $value)
 * @method self receiverContact(string $value)
 * @method self receiverPhone(string $value)
 * @method self countCargoPlaces(string $value)
 * @method self branch(string $value)
 * @method self weight(float $value)
 * @method self volume(float $value)
 * @method self deliveryType(int $value)
 * @method self codTransferType(int $value)
 * @method self codCardNumber(string $value)
 * @method self codBranch(string $value)
 * @method self declaredCost(float $value)
 * @method self deliveryAmount(float $value)
 * @method self redeliveryAmount(float $value)
 * @method self orderAmount(float $value)
 * @method self redeliveryPaymentIsRequired(bool $value)
 * @method self redeliveryPaymentPayer(int $value)
 * @method self deliveryPaymentIsRequired(bool $value)
 * @method self deliveryPaymentPayer(int $value)
 * @method self orderPaymentIsRequired(bool $value)
 * @method self addDescription(string $value)
 * @method self maxSize(int $value)
 * @method self dimensionalWeight(int $value)
 * @method self typeSize(string $value)
 * @method self deliveryIsRequired(bool $value)
 * @method self receiverCityId(string $value)
 * @method self receiverDeliveryAddress(string $value)
 * @method self deliveryInterval(string $value)
 * @method self safeDeal(bool $value)
 * */
class CreateDocumentRequest extends BaseRequest implements BeRequestEntity
{
    public const CARGO_TYPE_PARCEL = 'Parcel';
    public const CARGO_TYPE_CARGO = 'Cargo';
    public const CARGO_TYPE_PALLET = 'Pallet';
    public const CARGO_TYPE_TIRES_WHEELS = 'TiresWheels';

    public const DELIVERY_TYPE_B2C = 0;
    public const DELIVERY_TYPE_C2C = 1;
    public const DELIVERY_TYPE_B2B = 2;
    public const DELIVERY_TYPE_C2B = 3;

    public const COD_TRANSFER_TYPE_CASH = 0;
    public const COD_TRANSFER_TYPE_CARD = 1;
    public const COD_TRANSFER_TYPE_CA = 2;

    public const REDELIVERY_PAYMENT_PAYER_SENDER = 0;
    public const REDELIVERY_PAYMENT_PAYER_RECIPIENT = 1;
    public const REDELIVERY_PAYMENT_PAYER_THIRD_PERSON = 2;

    public const DELIVERY_PAYMENT_PAYER_SENDER = 0;
    public const DELIVERY_PAYMENT_PAYER_RECIPIENT = 1;
    public const DELIVERY_PAYMENT_PAYER_THIRD_PERSON = 2;

    /**
     * @param CargoPlace[]|string[] $value
     */
    public function cargoPlacesArray(array $value)
    {
        return $this->with('cargo_places_array', $value);
    }
}
