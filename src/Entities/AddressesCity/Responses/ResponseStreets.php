<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\AddressesCity\Responses;

use Flax\Contracts\BeResponseEntity;
use Flax\Entities\AddressesCity\Street;
use Illuminate\Support\Collection;

/**
 * */
class ResponseStreets extends BaseResponse implements BeResponseEntity
{
    /**
     * @return Street[]
     */
    public function data(): array
    {
        return self::getCollectOfData(Street::class);
    }

    public function dataCollect(): Collection
    {
        return self::getCollectOfData(Street::class, true);
    }

    public function isEmpty(): bool
    {
        return $this->dataCollect()->isEmpty();
    }
}
