<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Base\V2;

use Flax\Core\Builder\FilterBuilder;
use Flax\Core\Builder\FilterQuery;
use Flax\Core\Builder\ParamsBuilder;
use Flax\Entities\BaseRequest as Base;
use Flax\Exceptions\ValidateRequestException;

/**
 * @property string $request
 * @property string $type
 * @property string $name
 * @property string $language
 * @property int $TOP
 * @property FilterBuilder $filter
 * @property ParamsBuilder $params
 * */
abstract class BaseRequest extends Base
{
    public const LANG_UA = 'ua';
    public const LANG_RU = 'ru';
    public const LANG_EN = 'en';

    protected const DEF_COMPARISON = FilterQuery::EQUAL;

    public function __construct(array $data = [])
    {
        parent::__construct($data);

        $this->filter = new FilterBuilder();
        $this->params = new ParamsBuilder();
    }

    public function filter(): FilterBuilder
    {
        return $this->filter;
    }

    public function params(): ParamsBuilder
    {
        return $this->params;
    }

    public function validate(): bool
    {
        if (!$this->filter->validate()) {
            throw new ValidateRequestException('The filter query isn\'t valid');
        }

        return parent::validate();
    }

    public function __call($name, $arguments)
    {
        $comparison = $arguments[2] ?? self::DEF_COMPARISON;
        $leftValue = $arguments[0] ?? null;
        $rightValue = $arguments[1] ?? null;

        return $this->withFilter($name, $comparison, $leftValue, $rightValue);
    }

    protected function withFilter(string $name, string $comparison, $leftValue, $rightValue)
    {
        $this->filter->where($name, $comparison, $leftValue, $rightValue);

        return $this;
    }
}
