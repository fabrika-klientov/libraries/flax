<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Addresses\Additional;

use Flax\Entities\Addresses\Entity;

/**
 * @property-read string $Monday
 * @property-read string $Tuesday
 * @property-read string $Wednesday
 * @property-read string $Thursday
 * @property-read array $Friday
 * @property-read array $Saturday
 * @property-read array $Sunday
 * */
class Schedule extends Entity
{

}
