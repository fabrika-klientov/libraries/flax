<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Documents\Responses;

use Flax\Contracts\BeResponseEntity;
use Flax\Entities\Documents\Calculate;

/**
 * */
class ResponseCalculate extends BaseResponse implements BeResponseEntity
{
    /**
     * @return Calculate
     *
     * @throws \Flax\Exceptions\FlaxException
     */
    public function data(): Calculate
    {
        return self::factoryObjectWithThrow(Calculate::class);
    }
}
