<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Services;

use Flax\Client;
use Flax\Contracts\BeRequestEntity;
use Flax\Core\Http\HttpClient;
use Flax\Exceptions\HttpClientException;
use Flax\Exceptions\ValidateRequestException;
use Illuminate\Support\Collection;

abstract class BaseService
{
    protected $client;
    protected $useUserToken = false;
    protected $keysCollect = [];
    protected $keysEntity = [];

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * Prepared HttpClient
     *
     * @return HttpClient
     */
    protected function getHttpClient(): HttpClient
    {
        $client = $this->client->getHttpClient();
        $client->setUseType($this->useType());

        return $client;
    }

    /**
     * @param string $method
     * @param string $link
     * @param BeRequestEntity|Collection|array|null $request
     * @return array|null
     *
     * @throws HttpClientException
     * @throws ValidateRequestException
     */
    protected function doRequest(string $method, string $link, $request)
    {
        try {
            $data = self::requestData($request);
        } catch (ValidateRequestException $e) {
            $e->setMessage(sprintf($e->getMessage(), $link));

            throw $e;
        }

        $http = $this->getHttpClient();
        switch ($method) {
            case HttpClient::GET:
                return $http->get($link, $data);
            case HttpClient::POST:
                return $http->post($link, $data);
            default:
                throw new HttpClientException("Method [$method] is not resolved");
        }
    }

    /**
     * @param BeRequestEntity|Collection|array|null $request
     *
     * @throws ValidateRequestException
     */
    protected function requestData($request): array
    {
        switch (true) {
            case is_null($request):
                $data = [];
                break;

            case is_array($request):
                $data = $request;
                break;

            case $request instanceof BeRequestEntity:
                $data = self::_getSingleData($request);
                break;

            case $request instanceof Collection:
                if (!$request->every(function ($item) {
                    return $item instanceof BeRequestEntity;
                })) {
                    throw new ValidateRequestException(
                        'All items of list should be instance of BeRequestEntity [%s]'
                    );
                }

                $data = $request
                    ->map(function (BeRequestEntity $item) {
                        return self::_getSingleData($item);
                    })
                    ->values()
                    ->all();
                break;

            default:
                throw new ValidateRequestException('Unexpected type Request [%s]');
        }

        return $data;
    }

    /**
     * @param BeRequestEntity $request
     * @return array
     *
     * @throws ValidateRequestException
     */
    private function _getSingleData(BeRequestEntity $request): array
    {
        if (!$request->validate()) {
            throw new ValidateRequestException("Some params are required for this method [%s]");
        }

        return $request->getData();
    }

    /**
     * @param array $result
     * @param string $classEntity
     * @param array|null $keysCollect
     * @param array|null $keysEntity
     * @return Collection
     */
    protected function factoryEntitiesCollect(
        array $result,
        string $classEntity,
        array $keysCollect = null,
        array $keysEntity = null
    ): Collection {
        if (empty($result)) {
            return new Collection();
        }

        $keysCollect = $keysCollect ?? $this->keysCollect;
        foreach ($keysCollect as $key) {
            $result = $result[$key] ?? [];
        }

        return (new Collection($result))
            ->map(function ($item) use ($classEntity, $keysEntity) {
                return $this->factoryEntity($item, $classEntity, $keysEntity);
            });
    }

    /**
     * @param array $result
     * @param string $classEntity
     * @param array|null $keysEntity
     * @return mixed|null
     */
    protected function factoryEntity(array $result, string $classEntity, array $keysEntity = null)
    {
        if (empty($result)) {
            return null;
        }

        $keysEntity = $keysEntity ?? $this->keysEntity;
        foreach ($keysEntity as $key) {
            $result = $result[$key] ?? [];
        }

        return new $classEntity($result);
    }

    /**
     * @return int
     */
    abstract protected function useType(): int;
}
