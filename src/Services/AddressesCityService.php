<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Services;

use Flax\Core\Http\HttpClient;
use Flax\Entities\AddressesCity\Requests;
use Flax\Entities\AddressesCity\Responses;

class AddressesCityService extends BaseV3Service
{
    protected const ADDRESSES_CITIES = 'addresses/cities';
    protected const ADDRESSES_STREETS = 'addresses/streets';
    protected const ADDRESSES_BUILDINGS = 'addresses/buildings';

    public function getCitiesDelivery(Requests\CitiesDeliveryRequest $request = null): ?Responses\ResponseCitiesDelivery
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::ADDRESSES_CITIES, $request),
            Responses\ResponseCitiesDelivery::class
        );
    }

    public function getStreets(Requests\StreetsRequest $request): ?Responses\ResponseStreets
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::ADDRESSES_STREETS, $request),
            Responses\ResponseStreets::class
        );
    }

    public function getBuildings(Requests\BuildingsRequest $request): ?Responses\ResponseBuildings
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::GET, self::ADDRESSES_BUILDINGS, $request),
            Responses\ResponseBuildings::class
        );
    }

    protected function useType(): int
    {
        return HttpClient::USE_V3;
    }
}
