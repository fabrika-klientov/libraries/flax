<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Base\V2;

use Flax\Entities\Entity;

/**
 * @property-read bool $status
 * @property-read string $codeError
 * @property-read string $message
 * */
class ResponseResultEntity extends Entity
{
    public const MESSAGE_OK = 'OK';
    public const CODE_ERROR_OK = '777';
}
