<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Documents\Additional;

use Flax\Entities\Documents\Entity;

/**
 * @property-read string $marking
 * @property-read int $weight
 * @property-read int $width
 * @property-read int $height
 * @property-read int $depth
 * */
class CargoPlace extends Entity
{

}
