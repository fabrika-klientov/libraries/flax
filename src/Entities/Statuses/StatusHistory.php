<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Statuses;

use Flax\Contracts\BeEntity;
use Flax\Entities\Statuses\Additional\OrderSimple;
use Flax\Entities\Statuses\Additional\SenderSimple;
use Flax\Entities\Statuses\Additional\StatusOrderSimple;

/**
 * @property-read string $statusDate
 * @property-read array $order
 * @property-read array $senderId
 * @property-read array $statusOrder
 * @property-read string $statusCode
 * @property-read string $orderNumber
 * @property-read string $clientNumber
 * @property-read string $TTN
 * @property-read string $DeliveryDate
 * */
class StatusHistory extends FieldsEntity implements BeEntity
{
    public function order(): OrderSimple
    {
        return new OrderSimple($this->order);
    }

    public function sender(): SenderSimple
    {
        return new SenderSimple($this->senderId);
    }

    public function statusOrder(): StatusOrderSimple
    {
        return new StatusOrderSimple($this->statusOrder);
    }
}
