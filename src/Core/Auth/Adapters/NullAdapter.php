<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Core\Auth\Adapters;

use Flax\Contracts\BeAuthAdapter;

class NullAdapter implements BeAuthAdapter
{
    public function read(string $login): array
    {
        return [];
    }

    public function write(string $login, array $data): void
    {
        //
    }
}
