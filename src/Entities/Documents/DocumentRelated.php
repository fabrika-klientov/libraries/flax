<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Documents;

use Flax\Contracts\BeEntity;

/**
 * @property-read string $type
 * @property-read string $id
 * @property-read string $number
 * @property-read string $clientNumber
 * @property-read string $ttn
 * */
class DocumentRelated extends Entity implements BeEntity
{

}
