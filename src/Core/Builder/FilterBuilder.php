<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Core\Builder;

use Closure;
use Flax\Core\Classes\ActiveRecord;
use Flax\Exceptions\ValidateRequestException;

class FilterBuilder extends ActiveRecord
{
    /**
     * @param string|Closure|FilterQuery $name
     * @param string|null $comparison
     * @param mixed|null $leftValue
     * @param mixed|null $rightValue
     * @return FilterBuilder
     *
     * @throws ValidateRequestException
     */
    public function where($name, string $comparison = null, $leftValue = null, $rightValue = null)
    {
        switch (true) {
            case $name instanceof Closure:
                $filter = new FilterQuery();
                $name($filter);
                break;

            case $name instanceof FilterQuery:
                $filter = $name;
                break;

            case is_string($name):
                $filter = new FilterQuery($name, $comparison, $leftValue, $rightValue);
                break;

            default:
                throw new ValidateRequestException('Unexpected [name] type');
        }

        $this->data[] = $filter;

        return $this;
    }

    public function validate(): bool
    {
        foreach ($this->data as $item) {
            if (!$item->valid()) {
                return false;
            }
        }

        return true;
    }
}
