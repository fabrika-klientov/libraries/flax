<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Services;

use Flax\Core\Http\HttpClient;
use Flax\Entities\Registers\Requests;
use Flax\Entities\Registers\Responses;

class RegistersService extends BaseV1Service
{
    protected const DOCUMENTS_PICKUP_ADD = 'documents/pickup/add';
    protected const DOCUMENTS_PICKUP_GET = 'documents/pickup/get';
    protected const DOCUMENTS_PICKUP_DELETE = 'documents/pickup/del';
    protected const DOCUMENTS_PICKUP_CANCEL = 'documents/pickup/cancel';

    public function add(Requests\AddRegisterRequest $request, string $phone = null): ?Responses\ResponseAddRegister
    {
        return self::factoryEntity(
            self::doPackDataRequest(HttpClient::POST, self::DOCUMENTS_PICKUP_ADD, $request, ['phone' => $phone]),
            Responses\ResponseAddRegister::class
        );
    }

    public function list(Requests\ListRegistersRequest $request, string $phone = null): ?Responses\ResponseListRegisters
    {
        return self::factoryEntity(
            self::doPackDataRequest(HttpClient::POST, self::DOCUMENTS_PICKUP_GET, $request, ['phone' => $phone]),
            Responses\ResponseListRegisters::class
        );
    }

    public function removeDocumentsFromRegister(
        Requests\RemoveDocumentsFromRegisterRequest $request,
        string $phone = null
    ): ?Responses\ResponseRemoveDocumentsFromRegister {
        return self::factoryEntity(
            self::doPackDataRequest(HttpClient::POST, self::DOCUMENTS_PICKUP_DELETE, $request, ['phone' => $phone]),
            Responses\ResponseRemoveDocumentsFromRegister::class
        );
    }

    public function destroy(Requests\DestroyRegisterRequest $request): ?Responses\ResponseDestroyRegister
    {
        return self::factoryEntity(
            self::doPackDataRequest(HttpClient::POST, self::DOCUMENTS_PICKUP_CANCEL, $request),
            Responses\ResponseDestroyRegister::class
        );
    }

    protected function useType(): int
    {
        return HttpClient::USE_V1;
    }
}
