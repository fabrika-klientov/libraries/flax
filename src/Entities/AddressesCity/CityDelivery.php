<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\AddressesCity;

use Flax\Contracts\BeEntity;

/**
 * @property-read string $dbId
 * @property-read string $cityUUID
 * @property-read string $name
 * @property-read string $regionUUID
 * @property-read string $regionName
 * @property-read string $address
 * @property-read string $koatuu
 * */
class CityDelivery extends Entity implements BeEntity
{

}
