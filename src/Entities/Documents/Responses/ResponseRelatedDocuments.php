<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Documents\Responses;

use Flax\Contracts\BeResponseEntity;
use Flax\Entities\Documents\DocumentRelated;
use Illuminate\Support\Collection;

/**
 * */
class ResponseRelatedDocuments extends BaseResponse implements BeResponseEntity
{
    /**
     * @return DocumentRelated[]
     */
    public function data(): array
    {
        return self::getCollectOfData(DocumentRelated::class);
    }

    public function dataCollect(): Collection
    {
        return self::getCollectOfData(DocumentRelated::class, true);
    }

    public function isEmpty(): bool
    {
        return $this->dataCollect()->isEmpty();
    }
}
