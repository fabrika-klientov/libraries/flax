<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Registers\Responses;

use Flax\Contracts\BeResponseEntity;
use Flax\Entities\Registers\Register;
use Illuminate\Support\Collection;

/**
 * */
class ResponseListRegisters extends BaseResponse implements BeResponseEntity
{
    /**
     * @return Register[]
     */
    public function data(): array
    {
        return self::getCollectOfData(Register::class);
    }

    public function dataCollect(): Collection
    {
        return self::getCollectOfData(Register::class, true);
    }

    public function isEmpty(): bool
    {
        return $this->dataCollect()->isEmpty();
    }
}
