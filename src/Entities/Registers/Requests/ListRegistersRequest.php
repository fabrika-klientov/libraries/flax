<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Registers\Requests;

use Flax\Contracts\BeRequestEntity;

/**
 * @method self number(string $value)
 * @method self date(string $value)
 * */
class ListRegistersRequest extends BaseRequest implements BeRequestEntity
{

}
