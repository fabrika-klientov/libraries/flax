<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Documents\Responses;

use Flax\Contracts\BeResponseEntity;
use Flax\Entities\Documents\Additional\CargoPlaces;
use Flax\Entities\Documents\Document;

/**
 * @property-read array $cargo_places_data
 * */
class ResponseStoreDocument extends BaseResponse implements BeResponseEntity
{
    /**
     * @return Document
     *
     * @throws \Flax\Exceptions\FlaxException
     */
    public function data(): Document
    {
        return self::factoryObjectWithThrow(Document::class);
    }

    public function cargoPlacesData(): CargoPlaces
    {
        return new CargoPlaces($this->cargo_places_data ?? []);
    }
}
