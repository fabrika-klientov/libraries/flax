<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Flax
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax;

use Flax\Contracts\BeAuthAdapter;
use Flax\Core\Auth\AuthService;
use Flax\Core\Auth\Credentials;
use Flax\Core\Http\HttpClient;
use Flax\Services;

class Client
{
    private $authService;
    private $httpClient;

    public function __construct(Credentials $credentials, bool $testMode = false)
    {
        $this->authService = new AuthService($credentials, $testMode);
        $this->httpClient = new HttpClient($this->authService);
    }

    /**
     * @param BeAuthAdapter $authAdapter
     * @return Client
     */
    public function withAuthAdapter(BeAuthAdapter $authAdapter)
    {
        $this->authService->setAdapter($authAdapter);

        return $this;
    }

    /**
     * @return AuthService
     */
    public function getAuthService(): AuthService
    {
        return $this->authService;
    }

    /**
     * @return HttpClient
     */
    public function getHttpClient(): HttpClient
    {
        return $this->httpClient;
    }

    // additional services

    /**
     * @return Services\AddressesService
     */
    public function getAddressesService(): Services\AddressesService
    {
        return new Services\AddressesService($this);
    }

    /**
     * @return Services\AddressesCityService
     */
    public function getAddressesCityService(): Services\AddressesCityService
    {
        return new Services\AddressesCityService($this);
    }

    /**
     * @return Services\DocumentsService
     */
    public function getDocumentsService(): Services\DocumentsService
    {
        return new Services\DocumentsService($this);
    }

    /**
     * @return Services\RegistersService
     */
    public function getRegistersService(): Services\RegistersService
    {
        return new Services\RegistersService($this);
    }

    /**
     * @return Services\StatusesService
     */
    public function getStatusesService(): Services\StatusesService
    {
        return new Services\StatusesService($this);
    }

    /**
     * @return Services\AdditionalService
     */
    public function getAdditionalService(): Services\AdditionalService
    {
        return new Services\AdditionalService($this);
    }

    /**
     * @return Services\StickersService
     */
    public function getStickersService(): Services\StickersService
    {
        return new Services\StickersService($this);
    }
}
