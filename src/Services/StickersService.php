<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Services;

use Flax\Core\Http\HttpClient;
use Flax\Entities\Stickers\Requests;
use Flax\Exceptions\AuthException;

class StickersService extends BaseV1Service
{
    protected const PRINT_STICKER_ORDER = 'printSticker/order';
    protected const PRINT_PACKAGE_STICKERS = 'printPackageStickers/print';
    protected const PRINT_REGISTER = 'printReestr/reestr';
    protected const PRINT_REGISTER_STICKER = 'printRegisterSticker/Register';

    public function stickerDocument(string $number, Requests\StickerDocumentRequest $request = null): string
    {
        $link = self::getHttpClient()->getLink(self::PRINT_STICKER_ORDER);

        if ($request) {
            $requestData = $request->getData();
        }

        return self::withQuery($link, array_merge(['order_number' => $number], $requestData ?? []));
    }

    public function stickersListOfDocuments(array $numbers): string
    {
        return self::getHttpClient()
            ->request(
                HttpClient::POST,
                self::PRINT_PACKAGE_STICKERS,
                ['json' => ['order_number_list' => $numbers]],
                true
            );
    }

    public function stickerRegister(string $number): string
    {
        $link = self::getHttpClient()->getLink(self::PRINT_REGISTER);

        return self::withQuery($link, ['order_number' => $number]);
    }

    public function stickerDocumentsOfRegister(string $number): string
    {
        $link = self::getHttpClient()->getLink(self::PRINT_REGISTER_STICKER);

        return self::withQuery($link, ['register_number' => $number]);
    }

    /**
     * @throws AuthException
     */
    protected function withQuery(string $link, array $query, bool $withApikey = true): string
    {
        $link = preg_replace('/\/$/', '', $link);
        foreach ($query as $key => $value) {
            $link .= mb_strpos($link, '?') === false ? '?' : '&';
            $link .= "$key=$value";
        }

        if ($withApikey) {
            $credentials = self::getClient()->getAuthService()->getCredentials();
            if (!$credentials->isApikeyAuth()) {
                throw new AuthException('For create link [apikey] is required', AuthException::VALIDATE_AUTH_CODE);
            }

            $link .= mb_strpos($link, '?') === false ? '?' : '&';
            $link .= "api_key={$credentials->getApikey()}";
        }

        return $link;
    }

    protected function useType(): int
    {
        return HttpClient::USE_V1;
    }
}
