<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Documents\Requests;

/**
 * @method self marking(string $value)
 * @method self weight(float $value)
 * @method self width(float $value)
 * @method self height(float $value)
 * @method self depth(float $value)
 * */
class CargoPlace extends BaseRequest
{

}
