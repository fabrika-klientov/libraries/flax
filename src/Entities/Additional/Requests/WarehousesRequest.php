<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Additional\Requests;

use Flax\Contracts\BeRequestEntity;

/**
 * @method self senderId($value, $rightValue = null, string $comparison = self::DEF_COMPARISON)
 * */
class WarehousesRequest extends BaseRequest implements BeRequestEntity
{

}
