<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities;

use Flax\Core\Classes\ActiveRecord;

abstract class BaseRequest extends ActiveRecord
{
    public function getData(): array
    {
        return $this->data;
    }

    protected function with(string $key, $value)
    {
        $this->setData($key, $value);

        return $this;
    }

    protected function setData(string $key, $value)
    {
        $this->data[$key] = $value;
    }

    protected function remove(string $key)
    {
        unset($this->data[$key]);
    }

    /**
     * @throws \Flax\Exceptions\ValidateRequestException
     */
    public function validate(): bool
    {
        return true;
    }
}
