<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Core\Auth;

use Flax\Contracts\BeAuthAdapter;
use Flax\Core\Auth\Adapters\AuthFileJsonAdapter;
use Flax\Core\Http\HttpClient;
use Flax\Exceptions\AuthException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;

class AuthService
{
    protected const AUTH = 'auth/login';
    protected const REFRESH = 'auth/refresh';
    protected const LOGOUT = 'auth/logout';
    protected const CLOSE = 'auth/close';

    private $credentials;
    private $httpClient;
    /**
     * @var BeAuthAdapter $adapter
     * */
    private $adapter;

    protected $testMode;

    private $token;
    private $refreshToken;

    public function __construct(Credentials $credentials, bool $testMode = false)
    {
        $this->credentials = $credentials;
        $this->testMode = $testMode;
        $this->token = $credentials->getToken();
        $this->refreshToken = $credentials->getRefreshToken();
    }

    /**
     * @param BeAuthAdapter $adapter
     */
    public function setAdapter(BeAuthAdapter $adapter): void
    {
        $this->adapter = $adapter;
    }

    /**
     * @return Credentials
     */
    public function getCredentials(): Credentials
    {
        return $this->credentials;
    }

    /**
     * @return bool
     */
    public function isTestMode(): bool
    {
        return $this->testMode;
    }

    /**
     * @param bool $testMode
     */
    public function setTestMode(bool $testMode): void
    {
        $this->testMode = $testMode;
    }

    /**
     * @param HttpClient $httpClient
     */
    public function setHttpClient(HttpClient $httpClient): void
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @throws AuthException
     */
    public function auth(HttpClient $httpClient = null)
    {
        self::doRequest($httpClient, self::AUTH);
    }

    /**
     * @throws AuthException
     */
    public function refreshToken(HttpClient $httpClient = null)
    {
        self::doRequest($httpClient);
    }

    /**
     * @throws AuthException
     */
    public function logout(HttpClient $httpClient = null)
    {
        self::doRequest($httpClient, self::LOGOUT);
    }

    /**
     * @throws AuthException
     */
    public function close(HttpClient $httpClient = null)
    {
        self::doRequest($httpClient, self::CLOSE);
    }

    /**
     * @throws AuthException
     */
    public function loadToken(HttpClient $httpClient = null, bool $force = false): string
    {
        if ($force) {
            try {
                $this->refreshToken($httpClient);
            } catch (AuthException $e) {
                switch ($e->getCode()) {
                    // try to auth
                    case AuthException::VALIDATE_AUTH_CODE:
                        $this->auth($httpClient);
                        break;

                    default:
                        throw $e;
                }
            }

            return $this->token;
        }

        if (empty($this->token)) {
            $authData = self::getAdapter()->read($this->credentials->getLogin());
            if (empty($authData['accessToken'])) {
                return $this->loadToken($httpClient, true);
            }

            self::setTokens($authData);
        }

        return $this->token;
    }

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @throws AuthException
     */
    protected function doRequest(?HttpClient $httpClient, string $link = self::REFRESH)
    {
        $httpClient = self::checkHttpClientWithThrow($httpClient);
        $guzzleClient = $httpClient->getClient(HttpClient::USE_V3, false);

        switch ($link) {
            case self::AUTH:
            case self::CLOSE:
                if (!$this->credentials->isLoginAuth()) {
                    throw new AuthException(
                        'For auth OR close [login] and [password] are required',
                        AuthException::VALIDATE_AUTH_CODE
                    );
                }
                $json = ['login' => $this->credentials->getLogin(), 'password' => $this->credentials->getPassword()];
                break;

            case self::REFRESH:
                if (!$this->credentials->isRefreshTokenAuth()) {
                    throw new AuthException(
                        'For refresh token [refreshToken] is required',
                        AuthException::VALIDATE_AUTH_CODE
                    );
                }
                $json = ['refreshToken' => $this->refreshToken];
                break;

            case self::LOGOUT:
                if (!$this->credentials->isTokenAuth()) {
                    throw new AuthException('For logout token [token] is required', AuthException::VALIDATE_AUTH_CODE);
                }
                $headers = ['Authorization' => sprintf('Bearer %s', $this->credentials->getToken())];
                break;

            default:
                throw new AuthException("Unexpected link [$link].", AuthException::VALIDATE_AUTH_CODE);
        }

        try {
            $response = $guzzleClient->post($link, ['json' => $json ?? [], 'headers' => $headers ?? []]);

            $contents = $response->getBody()->getContents();
            $result = json_decode($contents, true);

            switch ($link) {
                case self::AUTH:
                case self::REFRESH:
                    if (!isset($result['accessToken'], $result['refreshToken'])) {
                        throw new AuthException('Server returned empty data');
                    }

                    self::setTokens($result);
                    self::getAdapter()->write($this->credentials->getLogin(), $result);

                    return $result;

                default:
                    return true;
            }
        } catch (ClientException $e) {
            throw new AuthException($e->getMessage(), $e->getCode());
        } catch (ServerException | GuzzleException $e) {
            throw new AuthException($e->getMessage(), $e->getCode());
        }
    }

    protected function setTokens(array $data)
    {
        $this->credentials->setToken($this->token = $data['accessToken']);
        $this->credentials->setRefreshToken($this->refreshToken = $data['refreshToken']);
    }

    /**
     * @throws AuthException
     */
    protected function checkHttpClientWithThrow(?HttpClient $httpClient): HttpClient
    {
        $httpClient = $httpClient ?? $this->httpClient;
        if (empty($httpClient)) {
            throw new AuthException('You should set HttpClient before get request');
        }

        return $httpClient;
    }

    /**
     * @return BeAuthAdapter
     */
    protected function getAdapter(): BeAuthAdapter
    {
        if (empty($this->adapter)) {
            $this->adapter = new AuthFileJsonAdapter();
        }

        return $this->adapter;
    }
}
