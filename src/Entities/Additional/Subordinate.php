<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Additional;

use Flax\Contracts\BeEntity;
use Flax\Entities\Additional\Additional\Counterparty;

/**
 * @property-read string $main_login
 * @property-read string $login
 * @property-read array $counterpart
 * */
class Subordinate extends FieldsEntity implements BeEntity
{
    public function counterpart(): Counterparty
    {
        return new Counterparty($this->counterpart);
    }
}
