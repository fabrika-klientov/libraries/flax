<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Documents;

use Flax\Contracts\BeEntity;

/**
 * @property-read string $number_en
 * @property-read string $number_kis
 * @property-read string $number_ttn
 * @property-read string $date
 * @property-read string $sender
 * @property-read string $reciver
 * @property-read string $phone_sender
 * @property-read string $phone_reciver
 * @property-read string $status
 * @property-read string $date_status
 * @property-read string $status_guid
 * */
class DocumentOfList extends Entity implements BeEntity
{

}
