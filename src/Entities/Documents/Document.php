<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Documents;

use Flax\Contracts\BeEntity;

/**
 * @property-read string $ttn
 * @property-read string $number
 * @property-read string $delivery_date
 * @property-read string $sender_address
 * @property-read string $reciver_address
 * @property-read string $reciver_city
 * @property-read string $reciver_warehouse
 * @property-read int $sort_id
 * @property-read int $cod_amount
 * @property-read int $delivery_amount_calculated
 * @property-read int $delivery_amount
 * @property-read string $branch_number
 * @property-read string $type
 * */
class Document extends Entity implements BeEntity
{

}
