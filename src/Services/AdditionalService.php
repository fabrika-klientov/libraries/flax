<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Services;

use Flax\Core\Http\HttpClient;
use Flax\Entities\Additional\Requests;
use Flax\Entities\Additional\Responses;

class AdditionalService extends BaseV2Service
{
    protected const TYPE_INFO_DATA = 'infoData';
    protected const TYPE_REQUEST = 'request';
    protected const TYPE_CATALOG = 'catalog';

    protected const NAME_SENDER_UUID = 'getSenderUUID';
    protected const NAME_SENDER_UUID_WITH_SUBORDINATES = 'getSenderUUIDWithSubordinates';
    protected const NAME_WAREHOUSE = 'cat_Warehouse';
    protected const NAME_CARGO_DESC = 'cat_CargoDescription';

    public function getSender(Requests\GetSenderRequest $request = null): ?Responses\ResponseSenders
    {
        if (empty($request)) {
            $request = new Requests\GetSenderRequest();
            $request->login($this->getClient()->getAuthService()->getCredentials()->getLogin());
        }

        return self::factoryEntity(
            self::getDataRequest(self::TYPE_INFO_DATA, self::NAME_SENDER_UUID, $request),
            Responses\ResponseSenders::class
        );
    }

    public function getSenderSubordinates(
        Requests\GetSenderSubordinatesRequest $request = null
    ): ?Responses\ResponseSenderSubordinates {
        if (empty($request)) {
            $request = new Requests\GetSenderSubordinatesRequest();
            $request->mainLogin($this->getClient()->getAuthService()->getCredentials()->getLogin());
        }

        return self::factoryEntity(
            self::getDataRequest(self::TYPE_REQUEST, self::NAME_SENDER_UUID_WITH_SUBORDINATES, $request),
            Responses\ResponseSenderSubordinates::class
        );
    }

    public function warehouses(Requests\WarehousesRequest $request = null): ?Responses\ResponseWarehouses
    {
        if (empty($request)) {
            $request = new Requests\WarehousesRequest();
            $request->senderId($this->getClient()->getAuthService()->getCredentials()->getApikey());
        }

        return self::factoryEntity(
            self::getDataRequest(self::TYPE_CATALOG, self::NAME_WAREHOUSE, $request),
            Responses\ResponseWarehouses::class
        );
    }

    public function cargoTypes(Requests\CargoTypesRequest $request = null): ?Responses\ResponseCargoTypes
    {
        return self::factoryEntity(
            self::getDataRequest(self::TYPE_CATALOG, self::NAME_CARGO_DESC, $request),
            Responses\ResponseCargoTypes::class
        );
    }

    protected function useType(): int
    {
        return HttpClient::USE_V2;
    }
}
