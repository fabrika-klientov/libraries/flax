<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Statuses\Requests;

use Flax\Contracts\BeRequestEntity;

/**
 * @method self senderId($value, $rightValue = null, string $comparison = self::DEF_COMPARISON)
 * @method self statusDate($value, $rightValue = null, string $comparison = self::DEF_COMPARISON)
 * @method self order($value, $rightValue = null, string $comparison = self::DEF_COMPARISON)
 * @method self statusOrder($value, $rightValue = null, string $comparison = self::DEF_COMPARISON)
 * @method self statusCode($value, $rightValue = null, string $comparison = self::DEF_COMPARISON)
 * @method self orderNumber($value, $rightValue = null, string $comparison = self::DEF_COMPARISON)
 * @method self clientNumber($value, $rightValue = null, string $comparison = self::DEF_COMPARISON)
 * @method self TTN($value, $rightValue = null, string $comparison = self::DEF_COMPARISON)
 * */
class StatusesHistoryRequest extends BaseRequest implements BeRequestEntity
{

}
