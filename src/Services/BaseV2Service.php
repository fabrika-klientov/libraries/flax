<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Services;

use Flax\Core\Http\HttpClient;
use Flax\Entities\Base\V2\BaseRequest;
use Flax\Entities\Base\V2\EmptyRequest;

abstract class BaseV2Service extends BaseService
{
    protected const METHOD_RUN_REQUEST = 'runRequest';
    protected const REQUEST_GET_DATA = 'getData';

    /**
     * @param string $type
     * @param string $name
     * @param BaseRequest|null $request
     * @return array|null
     *
     * @throws \Flax\Exceptions\HttpClientException
     * @throws \Flax\Exceptions\ValidateRequestException
     */
    protected function getDataRequest(string $type, string $name, ?BaseRequest $request)
    {
        $request = $request ?? new EmptyRequest();

        $request->request = self::REQUEST_GET_DATA;
        $request->type = $type;
        $request->name = $name;

        return $this->doRequest(HttpClient::POST, self::METHOD_RUN_REQUEST, $request);
    }
}
