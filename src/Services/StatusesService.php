<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Services;

use Flax\Core\Http\HttpClient;
use Flax\Entities\Statuses\Requests;
use Flax\Entities\Statuses\Responses;

class StatusesService extends BaseV2Service
{
    protected const TYPE_CATALOG = 'catalog';
    protected const TYPE_REQUEST = 'request';

    protected const NAME_STATUSES = 'orderStatuses';
    protected const NAME_STATUSES_HISTORY = 'getOrderStatusesHistoryF';

    public function statuses(Requests\StatusesRequest $request = null): ?Responses\ResponseStatuses
    {
        return self::factoryEntity(
            self::getDataRequest(self::TYPE_CATALOG, self::NAME_STATUSES, $request),
            Responses\ResponseStatuses::class
        );
    }

    public function statusesHistory(Requests\StatusesHistoryRequest $request): ?Responses\ResponseStatusesHistory
    {
        return self::factoryEntity(
            self::getDataRequest(self::TYPE_REQUEST, self::NAME_STATUSES_HISTORY, $request),
            Responses\ResponseStatusesHistory::class
        );
    }

    protected function useType(): int
    {
        return HttpClient::USE_V2;
    }
}
