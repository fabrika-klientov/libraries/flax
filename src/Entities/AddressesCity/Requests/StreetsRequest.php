<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\AddressesCity\Requests;

use Flax\Contracts\BeRequestEntity;

/**
 * @method self city(string $value)
 * @method self filter(string $value)
 * @method self take(string $value)
 * @method self lang(string $value)
 * */
class StreetsRequest extends BaseRequest implements BeRequestEntity
{

}
