<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Additional\Additional;

use Flax\Entities\Additional\Entity;

/**
 * @property-read string $uuid
 * @property-read string $descr
 * @property-read string $type
 * */
class Depart extends Entity
{

}
