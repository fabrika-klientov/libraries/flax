<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Base\V2;

use Flax\Entities\Entity;
use Flax\Exceptions\FlaxException;
use Illuminate\Support\Collection;

/**
 * @property-read array $response
 * @property-read array $data
 * @property-read int $totalCountRecords
 * */
abstract class ResponseEntity extends Entity
{
    /**
     * @return ResponseResultEntity
     */
    public function response(): ResponseResultEntity
    {
        return new ResponseResultEntity($this->response);
    }

    /**
     * @return int
     */
    public function totalCountRecords(): int
    {
        return $this->totalCountRecords ?? 0;
    }

    /**
     * @return mixed
     */
    abstract public function data();

    /**
     * @param string $className
     * @param bool $isCollection
     * @return array|Collection
     *
     * @throws FlaxException
     */
    protected function getCollectOfData(string $className, bool $isCollection = false)
    {
        if (!class_exists($className)) {
            throw new FlaxException("Class name [$className] is not resolved");
        }

        $list = array_map(function ($item) use ($className) {
            return new $className($item);
        }, $this->getData());

        if ($isCollection) {
            return new Collection($list);
        }

        return $list;
    }

    /** Via magic doesn't correct (inner the class)
     */
    protected function getData()
    {
        return $this->data['data'] ?? [];
    }
}
