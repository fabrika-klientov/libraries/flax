<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Documents;

use Flax\Contracts\BeEntity;

/**
 * @property-read string $point_a_locality_uuid
 * @property-read string $point_a_branch
 * @property-read string $point_b_locality_uuid
 * @property-read string $point_b_branch
 * @property-read string $weight
 * @property-read string $delivery
 * @property-read string $cargo_type
 * @property-read int $cod_transfer_type
 * @property-read int $max_length
 * @property-read string $cod
 * @property-read string $estcost
 * @property-read string $size
 * @property-read int $category_delivery
 * @property-read int $category_delivery_zone
 * @property-read string $price_insurance
 * @property-read string $SKU
 * @property-read float $price
 * @property-read float $price_cod
 * @property-read float $price_delivery
 * */
class Calculate extends Entity implements BeEntity
{

}
