<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Core\Builder;

use Flax\Core\Classes\ActiveRecord;

/**
 * @property string $name
 * @property string $comparison
 * @property string|array $leftvalue
 * @property string|array $rightvalue
 * */
class FilterQuery extends ActiveRecord
{
    public const EQUAL = 'equal';
    public const NOT = 'not';
    public const LESS = 'less';
    public const MORE = 'more';
    public const IN = 'in';
    public const BETWEEN = 'between';
    public const NOT_IN = 'not in';
    public const LESS_OR_EQUAL = 'less or equal';
    public const MORE_OR_EQUAL = 'more or equal';
    public const LIKE = 'like';

    public function __construct(string $name = null, string $comparison = null, $leftValue = null, $rightValue = null)
    {
        parent::__construct();

        if (isset($name, $comparison, $leftValue)) {
            $this->where($name, $comparison, $leftValue, $rightValue);
        }
    }

    /**
     * @param string $name
     * @param string $value
     * @return FilterQuery
     */
    public function equal(string $name, $value)
    {
        $this->where($name, self::EQUAL, $value);

        return $this;
    }

    /**
     * @param string $name
     * @param string $value
     * @return FilterQuery
     */
    public function not(string $name, $value)
    {
        $this->where($name, self::NOT, $value);

        return $this;
    }

    /**
     * @param string $name
     * @param string $value
     * @return FilterQuery
     */
    public function less(string $name, $value)
    {
        $this->where($name, self::LESS, $value);

        return $this;
    }

    /**
     * @param string $name
     * @param string $value
     * @return FilterQuery
     */
    public function more(string $name, $value)
    {
        $this->where($name, self::MORE, $value);

        return $this;
    }

    /**
     * @param string $name
     * @param array $value
     * @return FilterQuery
     */
    public function in(string $name, $value)
    {
        $this->where($name, self::IN, $value);

        return $this;
    }

    /**
     * @param string $name
     * @param string $leftValue
     * @param string $rightValue
     * @return FilterQuery
     */
    public function between(string $name, $leftValue, $rightValue)
    {
        $this->where($name, self::BETWEEN, $leftValue, $rightValue);

        return $this;
    }

    /**
     * @param string $name
     * @param array $value
     * @return FilterQuery
     */
    public function notIn(string $name, $value)
    {
        $this->where($name, self::NOT_IN, $value);

        return $this;
    }

    /**
     * @param string $name
     * @param string $value
     * @return FilterQuery
     */
    public function lessOrEqual(string $name, $value)
    {
        $this->where($name, self::LESS_OR_EQUAL, $value);

        return $this;
    }

    /**
     * @param string $name
     * @param string $value
     * @return FilterQuery
     */
    public function moreOrEqual(string $name, $value)
    {
        $this->where($name, self::MORE_OR_EQUAL, $value);

        return $this;
    }

    /**
     * @param string $name
     * @param string $value
     * @return FilterQuery
     */
    public function like(string $name, $value)
    {
        $this->where($name, self::LIKE, $value);

        return $this;
    }

    /**
     * @param string $name
     * @param string $comparison
     * @param mixed $leftValue
     * @param mixed $rightValue
     */
    public function where(string $name, string $comparison, $leftValue, $rightValue = null)
    {
        $this->name = $name;
        $this->comparison = $comparison;
        $this->leftvalue = $leftValue;
        if (isset($rightValue)) {
            $this->rightvalue = $rightValue;
        }
    }

    public function valid(): bool
    {
        return isset($this->name, $this->comparison, $this->leftvalue);
    }
}
