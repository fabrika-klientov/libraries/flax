<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Contracts
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Contracts;

interface BeRequestEntity
{
    public function getData(): array;

    /**
     * @return bool
     *
     * @throws \Flax\Exceptions\ValidateRequestException
     */
    public function validate(): bool;
}
