<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Documents;

use Flax\Contracts\BeEntity;
use Flax\Entities\Documents\Additional\ChangeLine;

/**
 * @property-read array $receiver_phone
 * @property-read array $receiver_contact
 * @property-read array $order_payment_amount
 * */
class DocumentChange extends Entity implements BeEntity
{
    public function receiverPhone(): ?ChangeLine
    {
        return empty($this->receiver_phone) ? null : new ChangeLine($this->receiver_phone);
    }

    public function receiverContact(): ?ChangeLine
    {
        return empty($this->receiver_contact) ? null : new ChangeLine($this->receiver_contact);
    }

    public function orderPaymentAmount(): ?ChangeLine
    {
        return empty($this->order_payment_amount) ? null : new ChangeLine($this->order_payment_amount);
    }
}
