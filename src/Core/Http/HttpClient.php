<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Core\Http;

use Flax\Core\Auth\AuthService;
use Flax\Exceptions\AuthException;
use Flax\Exceptions\HttpClientException;
use Flax\Exceptions\NotFoundException;
use Flax\Exceptions\ResponseErrorException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

class HttpClient
{
    public const USE_V1 = 1;
    public const USE_V2 = 2;
    public const USE_V3 = 3;

    public const API_URL = 'https://api.justin.ua/';
    public const API_URL_TEST = 'https://api.sandbox.justin.ua/';

    public const API_PREFIX = 'justin_pms/hs/';
    public const API_PREFIX_TEST = 'client_api/hs/';

    public const API_PREFIX_V3 = 'client_api/';
    public const API_PREFIX_V3_TEST = 'client_api/';

    public const API_VER_1 = 'api/v1/';
    public const API_VER_2 = 'v2/';
    public const API_VER_3 = 'v3/';

    protected const TIMEOUT = 30;

    public const GET = 'GET';
    public const POST = 'POST';

    /**
     * @var AuthService $authService
     * */
    private $authService;

    /**
     * @var int $useType
     * */
    private $useType;

    /**
     * @param AuthService $authService
     */
    public function __construct(AuthService $authService, int $use = self::USE_V2)
    {
        $this->authService = $authService;
        $this->useType = $use;
        $this->authService->setHttpClient($this);
    }

    /**
     * @param int $useType
     */
    public function setUseType(int $useType): void
    {
        $this->useType = $useType;
    }

    /** api GET
     * @param string $link
     * @param array $query
     * @param array $options
     * @return array|null
     *
     * @throws HttpClientException
     */
    public function get(string $link, array $query = [], array $options = [])
    {
        return $this->doRequest(
            self::GET,
            $link,
            array_merge($options, ['query' => $query])
        );
    }

    /** api POST
     * @param string $link
     * @param array $data
     * @param array $options
     * @return array|null
     *
     * @throws HttpClientException
     */
    public function post(string $link, array $data, array $options = [])
    {
        return $this->doRequest(
            self::POST,
            $link,
            array_merge($options, empty($data) ? [] : ['json' => $data])
        );
    }

    /**
     * @param string $link
     * @param array $options
     * @param bool $src
     * @return array|string|null
     *
     * @throws HttpClientException
     */
    public function request(string $method, string $link, array $options, bool $src = false)
    {
        return $this->doRequest($method, $link, $options, $src);
    }

    /**
     * @param int|null $use
     * @param bool $withAuth
     * @return Client
     *
     * @throws AuthException
     */
    public function getClient(int $use = null, bool $withAuth = true): Client
    {
        $use = $use ?? $this->useType;

        $baseUri = self::getLink('');

        $options = [
            'base_uri' => $baseUri,
            'headers' => [
                'Accept' => 'application/json',
            ],
            'timeout' => self::TIMEOUT,
        ];

        if ($withAuth) {
            $options = self::injectAuth($options, $use);
        }

        return new Client($options);
    }

    /**
     * @param string $method
     * @param string $link
     * @param array $options
     * @param int $prevCode
     * @return array|string|null
     *
     * @throws HttpClientException|AuthException
     * */
    protected function doRequest(string $method, string $link, array $options, bool $src = false, int $prevCode = 0)
    {
        try {
            $response = $this
                ->getClient()
                ->request($method, $this->getLink($link), self::prepareOptions($method, $link, $options));

            $content = $response->getBody()->getContents();
            if ($src) {
                return $content;
            }

            $result = json_decode($content, true);

            return $result;
        } catch (ClientException $e) {
            $code = $e->getCode();
            switch ($code) {
                case 401:
                    switch ($this->useType) {
                        case self::USE_V3:
                            if ($prevCode != $code) {
                                $this->authService->loadToken(null, true);
                                return $this->doRequest($method, $link, $options, $src, $code);
                            }
                            break;
                    }
                    break;
                case 404:
                    throw new NotFoundException('Data not found. ' . $e->getMessage(), $code);
                case 400:
                    $exception = new ResponseErrorException('Response error. ' . $e->getMessage(), $code);
                    $exception->setDataError(json_decode($e->getResponse()->getBody()->getContents() ?: '[]', true));

                    throw $exception;
            }

            throw new HttpClientException('Request returned error. ' . $e->getMessage(), $code);
        } catch (ServerException $e) {
            throw new HttpClientException('Request returned error. ' . $e->getMessage(), $e->getCode());
        } catch (\Throwable $e) {
            throw new HttpClientException('Request returned error. ' . $e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param string $link
     * @param int|null $use
     * @return string
     */
    public function getLink(string $link = '', int $use = null): string
    {
        $use = $use ?? $this->useType;
        $testMode = self::isTestMode();

        $uri = $testMode ? self::API_URL_TEST : self::API_URL;
        switch ($use) {
            case self::USE_V1:
                $uri .= $testMode ? self::API_PREFIX_TEST : self::API_PREFIX;
                $uri .= self::API_VER_1;
                break;

            case self::USE_V3:
                $uri .= $testMode ? self::API_PREFIX_V3_TEST : self::API_PREFIX_V3;
                $uri .= self::API_VER_3;
                break;

            default:
                $uri .= $testMode ? self::API_PREFIX_TEST : self::API_PREFIX;
                $uri .= self::API_VER_2;
        }

        if ($link) {
            $uri = self::_prepareLinkDeep($uri, $link, $use);
        }

        return $uri;
    }

    private function _prepareLinkDeep(string $uri, string $link, int $use): string
    {
        $testMode = self::isTestMode();

        $link = preg_replace('/^\//', '', $link);
        $uri .= $link;

        return $uri;
    }

    /**
     * @param string $method
     * @param string $link
     * @param array $options
     * @return array
     *
     * @throws AuthException
     */
    protected function prepareOptions(string $method, string $link, array $options): array
    {
        // prepare headers
        $headers = $options['headers'] ?? [];
        switch ($method) {
            case self::POST:
                if (empty($headers['Content-Type'])) {
                    $headers['Content-Type'] = 'application/json';
                }
                break;
        }

        $options['headers'] = $headers;

        // prepare body, query
        switch ($this->useType) {
            case self::USE_V1:
                if (!$this->authService->getCredentials()->isApikeyAuth()) {
                    throw new AuthException('For api/v1 [apikey] is required', AuthException::VALIDATE_AUTH_CODE);
                }

                switch ($method) {
                    case self::POST:
                        $json = $options['json'] ?? [];
                        $json['api_key'] = $this->authService->getCredentials()->getApikey();
                        $options['json'] = $json;
                        break;

                    case self::GET:
                        $query = $options['query'] ?? [];
                        $query['api_key'] = $this->authService->getCredentials()->getApikey();
                        $options['query'] = $query;
                }
                break;

            case self::USE_V2:
                if (!$this->authService->getCredentials()->isLoginAuth()) {
                    throw new AuthException(
                        'For api v2 [login] and [password] are required',
                        AuthException::VALIDATE_AUTH_CODE
                    );
                }

                switch ($method) {
                    case self::POST:
                        $json = $options['json'] ?? [];
                        $json['keyAccount'] = $this->authService->getCredentials()->getLogin();
                        $json['sign'] = sha1($this->authService->getCredentials()->getPassword() . ':' . date('Y-m-d'));
                        $options['json'] = $json;
                        break;
                }
        }

        return $options;
    }

    /**
     * @param array $options
     * @param int $use
     * @return array
     *
     * @throws AuthException
     */
    protected function injectAuth(array $options, int $use): array
    {
        $headers = $options['headers'] ?? [];

        switch ($use) {
            case self::USE_V3:
                $headers['Authorization'] = sprintf('Bearer %s', $this->authService->loadToken());
                break;
        }

        $options['headers'] = $headers;

        return $options;
    }

    /**
     * @return bool
     */
    protected function isTestMode(): bool
    {
        return $this->authService->isTestMode();
    }
}
