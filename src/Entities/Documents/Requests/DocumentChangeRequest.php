<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Documents\Requests;

use Flax\Contracts\BeRequestEntity;

/**
 * @method self receiverPhone(string $value)
 * @method self receiverContact(string $value)
 * @method self orderPaymentAmount(string $value)
 * */
class DocumentChangeRequest extends BaseRequest implements BeRequestEntity
{

}
