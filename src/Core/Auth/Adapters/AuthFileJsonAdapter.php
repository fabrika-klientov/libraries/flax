<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Core\Auth\Adapters;

use Flax\Contracts\BeAuthAdapter;

class AuthFileJsonAdapter implements BeAuthAdapter
{
    /**
     * @var string $path
     * */
    protected $path;

    protected const RELATIVE_PATH = ['storage', 'auth', 'flax'];

    public function __construct(string $path = null)
    {
        if (empty($path)) {
            self::defaultPath();
        }

        self::checkPath();
    }

    public function read(string $login): array
    {
        if (!file_exists(self::getPath($login))) {
            return [];
        }

        $data = file_get_contents(self::getPath($login));
        if (empty($data)) {
            return [];
        }

        $data = json_decode($data, true);
        if (empty($data)) {
            return [];
        }

        return $data;
    }

    public function write(string $login, array $data): void
    {
        $file = self::getPath($login);
        file_put_contents($file, json_encode($data));
        chmod($file, 0664);
    }

    protected function getPath(string $sub, string $ext = '.json'): string
    {
        return $this->path . DIRECTORY_SEPARATOR . $sub . $ext;
    }

    protected function checkPath()
    {
        if (!file_exists($this->path)) {
            if (!mkdir($this->path, 0775, true)) {
                throw new \RuntimeException('Didn\'t stored path for auth. Permission denied.');
            }
        }
    }

    protected function defaultPath()
    {
        $this->path = getcwd();
        $this->path .= DIRECTORY_SEPARATOR . join(DIRECTORY_SEPARATOR, self::RELATIVE_PATH);
    }
}
