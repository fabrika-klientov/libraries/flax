<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Addresses\Responses;

use Flax\Contracts\BeResponseEntity;
use Flax\Entities\Addresses\City;
use Illuminate\Support\Collection;

class ResponseCities extends BaseResponse implements BeResponseEntity
{
    /**
     * @return City[]
     */
    public function data(): array
    {
        return self::getCollectOfData(City::class);
    }

    public function dataCollect(): Collection
    {
        return self::getCollectOfData(City::class, true);
    }

    public function isEmpty(): bool
    {
        return $this->dataCollect()->isEmpty();
    }
}
