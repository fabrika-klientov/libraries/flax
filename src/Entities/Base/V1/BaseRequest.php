<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Base\V1;

use Flax\Entities\BaseRequest as Base;
use Illuminate\Support\Str;

/**
 * */
abstract class BaseRequest extends Base
{
    public function __call($name, $arguments)
    {
        return $this->with(Str::snake($name), $arguments[0]);
    }
}
