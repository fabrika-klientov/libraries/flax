<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\AddressesCity;

use Flax\Contracts\BeEntity;

/**
 * @property-read string $buildingNo
 * @property-read string $buildingId
 * @property-read string $buildingGuid
 * @property-read string $cityId
 * @property-read string $koatuu
 * @property-read string $cityName
 * @property-read string $streetId
 * @property-read string $streetGuid
 * @property-read string $streetName
 * @property-read string $address
 * */
class Building extends Entity implements BeEntity
{

}
