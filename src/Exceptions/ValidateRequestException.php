<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Exceptions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Exceptions;

class ValidateRequestException extends FlaxException
{

}
