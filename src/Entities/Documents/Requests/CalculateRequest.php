<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Documents\Requests;

use Flax\Contracts\BeRequestEntity;

/**
 * @method self pointABranch(string $value)
 * @method self pointBBranch(string $value)
 * @method self pointALocalityUuid(string $value)
 * @method self pointBLocalityUuid(string $value)
 * @method self weight(float $value)
 * @method self maxLength(float $value)
 * @method self cod(float $value)
 * @method self estcost(float $value)
 * @method self cargoType(string $value)
 * @method self codTransferType(int $value)
 * @method self delivery(int $value)
 * @method self cargoDetails(CargoDetail[] $value)
 * @method self cargoPlacesArray(CargoPlace[] $value)
 * */
class CalculateRequest extends BaseRequest implements BeRequestEntity
{

}
