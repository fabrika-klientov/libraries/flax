<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Additional\Responses;

use Flax\Contracts\BeResponseEntity;
use Flax\Entities\Additional\Sender;
use Illuminate\Support\Collection;

class ResponseSenders extends BaseResponse implements BeResponseEntity
{
    /**
     * @return Sender[]
     */
    public function data(): array
    {
        return self::getCollectOfData(Sender::class);
    }

    public function dataCollect(): Collection
    {
        return self::getCollectOfData(Sender::class, true);
    }

    public function isEmpty(): bool
    {
        return $this->dataCollect()->isEmpty();
    }
}
