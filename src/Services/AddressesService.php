<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Services;

use Flax\Core\Http\HttpClient;
use Flax\Entities\Addresses\Requests;
use Flax\Entities\Addresses\Responses;

class AddressesService extends BaseV2Service
{
    protected const TYPE_CATALOG = 'catalog';
    protected const TYPE_REQUEST = 'request';

    protected const NAME_REGION = 'cat_Region';
    protected const NAME_AREAS_REGION = 'cat_areasRegion';
    protected const NAME_CITIES = 'cat_Cities';
    protected const NAME_ADDRESS_DELIVERY = 'cat_addressDelivery';
    protected const NAME_CITY_REGIONS = 'cat_cityRegions';
    protected const NAME_CITY_STREET = 'cat_cityStreets';
    protected const NAME_BRANCH_TYPE = 'cat_branchType';
    protected const NAME_DEPARTMENTS_LANG = 'req_DepartmentsLang';

    public function getAreas(Requests\AreasRequest $request = null): ?Responses\ResponseAreas
    {
        return self::factoryEntity(
            self::getDataRequest(self::TYPE_CATALOG, self::NAME_REGION, $request),
            Responses\ResponseAreas::class
        );
    }

    public function getRegions(Requests\RegionsRequest $request = null): ?Responses\ResponseRegions
    {
        return self::factoryEntity(
            self::getDataRequest(self::TYPE_CATALOG, self::NAME_AREAS_REGION, $request),
            Responses\ResponseRegions::class
        );
    }

    public function getCities(Requests\CitiesRequest $request = null): ?Responses\ResponseCities
    {
        return self::factoryEntity(
            self::getDataRequest(self::TYPE_CATALOG, self::NAME_CITIES, $request),
            Responses\ResponseCities::class
        );
    }

    public function getAddressesDelivery(
        Requests\AddressesDeliveryRequest $request = null
    ): ?Responses\ResponseAddressesDelivery {
        return self::factoryEntity(
            self::getDataRequest(self::TYPE_REQUEST, self::NAME_ADDRESS_DELIVERY, $request),
            Responses\ResponseAddressesDelivery::class
        );
    }

    public function getCityRegions(Requests\CityRegionsRequest $request = null): ?Responses\ResponseCityRegions
    {
        return self::factoryEntity(
            self::getDataRequest(self::TYPE_CATALOG, self::NAME_CITY_REGIONS, $request),
            Responses\ResponseCityRegions::class
        );
    }

    public function getCityStreets(Requests\CityStreetsRequest $request): ?Responses\ResponseCityStreets
    {
        return self::factoryEntity(
            self::getDataRequest(self::TYPE_CATALOG, self::NAME_CITY_STREET, $request),
            Responses\ResponseCityStreets::class
        );
    }

    public function getBranchTypes(Requests\BranchTypesRequest $request = null): ?Responses\ResponseBranchTypes
    {
        return self::factoryEntity(
            self::getDataRequest(self::TYPE_CATALOG, self::NAME_BRANCH_TYPE, $request),
            Responses\ResponseBranchTypes::class
        );
    }

    public function getDepartments(Requests\DepartmentsRequest $request = null): ?Responses\ResponseDepartments
    {
        return self::factoryEntity(
            self::getDataRequest(self::TYPE_REQUEST, self::NAME_DEPARTMENTS_LANG, $request),
            Responses\ResponseDepartments::class
        );
    }

    // api v3 methods

    public function getAddressesCityService(): AddressesCityService
    {
        return $this->getClient()->getAddressesCityService();
    }

    protected function useType(): int
    {
        return HttpClient::USE_V2;
    }
}
