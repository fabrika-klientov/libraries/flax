<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\AddressesCity\Responses;

use Flax\Contracts\BeResponseEntity;
use Flax\Entities\AddressesCity\Building;
use Illuminate\Support\Collection;

/**
 * */
class ResponseBuildings extends BaseResponse implements BeResponseEntity
{
    /**
     * @return Building[]
     */
    public function data(): array
    {
        return self::getCollectOfData(Building::class);
    }

    public function dataCollect(): Collection
    {
        return self::getCollectOfData(Building::class, true);
    }

    public function isEmpty(): bool
    {
        return $this->dataCollect()->isEmpty();
    }
}
