<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Documents;

use Flax\Contracts\BeEntity;
use Flax\Entities\Documents\Additional\CargoPlace;

/**
 * @property-read string $number
 * @property-read string $date
 * @property-read string $sender_city_id
 * @property-read string $sender_company
 * @property-read string $sender_contact
 * @property-read string $sender_phone
 * @property-read string $sender_pick_up_address
 * @property-read string $sender_pickup_point
 * @property-read bool $pick_up_is_required
 * @property-read string $sender_branch
 * @property-read string $receiver
 * @property-read string $receiver_contact
 * @property-read string $receiver_phone
 * @property-read int $count_cargo_places
 * @property-read string $branch
 * @property-read float $volume
 * @property-read float $weight
 * @property-read float $declared_cost
 * @property-read float $delivery_amount
 * @property-read float $redelivery_amount
 * @property-read float $order_amount
 * @property-read bool $redelivery_payment_is_required
 * @property-read int $redelivery_payment_payer
 * @property-read bool $delivery_payment_is_required
 * @property-read string $add_description
 * @property-read int $delivery_payment_payer
 * @property-read bool $order_payment_is_required
 * @property-read int $delivery_type
 * @property-read int $cod_transfer_type
 * @property-read int $max_size
 * @property-read string $cod_card_number
 * @property-read array $cargo_places_array
 * @property-read string $cargo_type
 * @property-read array $cargo_details
 * @property-read string $reciver_city
 * @property-read string $reciver_address
 * */
class DocumentDetail extends Entity implements BeEntity
{
    /**
     * @return CargoPlace[]
     */
    public function cargoPlaces(): array
    {
        return array_map(function ($item) {
            return new CargoPlace($item);
        }, $this->cargo_places_array ?? []);
    }
}
