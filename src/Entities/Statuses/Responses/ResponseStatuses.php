<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Statuses\Responses;

use Flax\Contracts\BeResponseEntity;
use Flax\Entities\Statuses\Status;
use Illuminate\Support\Collection;

class ResponseStatuses extends BaseResponse implements BeResponseEntity
{
    /**
     * @return Status[]
     */
    public function data(): array
    {
        return self::getCollectOfData(Status::class);
    }

    public function dataCollect(): Collection
    {
        return self::getCollectOfData(Status::class, true);
    }

    public function isEmpty(): bool
    {
        return $this->dataCollect()->isEmpty();
    }
}
