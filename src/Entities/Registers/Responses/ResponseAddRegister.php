<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Registers\Responses;

use Flax\Contracts\BeResponseEntity;
use Flax\Entities\Registers\RegisterSimple;

/**
 * */
class ResponseAddRegister extends BaseResponse implements BeResponseEntity
{
    /**
     * @return RegisterSimple
     *
     * @throws \Flax\Exceptions\FlaxException
     */
    public function data(): RegisterSimple
    {
        return self::factoryObjectWithThrow(RegisterSimple::class);
    }
}
