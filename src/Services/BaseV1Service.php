<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Services;

use Flax\Exceptions\ValidateRequestException;

abstract class BaseV1Service extends BaseService
{
    /**
     * @param string $method
     * @param string $link
     * @param \Flax\Contracts\BeRequestEntity|\Illuminate\Support\Collection|array|null $request
     * @param array $additional
     * @return array|null
     *
     * @throws ValidateRequestException
     * @throws \Flax\Exceptions\HttpClientException
     */
    protected function doPackDataRequest(string $method, string $link, $request, array $additional = [])
    {
        try {
            return $this->doRequest($method, $link, array_merge(['data' => self::requestData($request)], $additional));
        } catch (ValidateRequestException $e) {
            $e->setMessage(sprintf($e->getMessage(), $link));

            throw $e;
        }
    }
}
