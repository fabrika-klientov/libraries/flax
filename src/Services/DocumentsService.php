<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Services
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Services;

use Flax\Core\Http\HttpClient;
use Flax\Entities\Documents\Requests;
use Flax\Entities\Documents\Responses;

class DocumentsService extends BaseV1Service
{
    protected const DOCUMENTS_ORDERS = 'documents/orders';
    protected const DOCUMENTS_ORDERS_CANCEL = 'documents/orders_cancel';
    protected const DOCUMENTS_LIST_ORDERS = 'documents/getListOrders';
    protected const DOCUMENTS_ORDER_INFO = 'documents/getOrderInfo';
    protected const DOCUMENTS_RELATED_ORDERS = 'documents/GetRelatedOrders';
    protected const DOCUMENTS_ORDERS_CHECK = 'documents/orders_check';
    protected const DOCUMENTS_ORDERS_CHANGE = 'documents/orders_change';
    protected const DOCUMENTS_RETURN_ORDERS_CHECK = 'documents/CheckPossibilityCreateReturn';
    protected const DOCUMENTS_CREATE_RETURN = 'documents/CreateReturn';
    protected const DOCUMENTS_REDIRECTING_CHECK = 'documents/CheckPossibilityCreateRedirecting';
    protected const DOCUMENTS_REDIRECTING_CREATE = 'documents/CreateRedirecting';
    protected const DOCUMENTS_SET_AGENT_IN_ORDER = 'documents/setAgentInOrder';
    protected const DOCUMENTS_CALCULATE_PRICE = 'documents/CALCULATEPRICE';

    public function create(Requests\CreateDocumentRequest $request): ?Responses\ResponseStoreDocument
    {
        return self::factoryEntity(
            self::doPackDataRequest(HttpClient::POST, self::DOCUMENTS_ORDERS, $request),
            Responses\ResponseStoreDocument::class
        );
    }

    public function cancel(string $number): ?Responses\ResponseCancelDocument
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::DOCUMENTS_ORDERS_CANCEL, ['number' => $number]),
            Responses\ResponseCancelDocument::class
        );
    }

    public function list(Requests\DocumentsListRequest $request): ?Responses\ResponseDocumentsList
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::DOCUMENTS_LIST_ORDERS, $request),
            Responses\ResponseDocumentsList::class
        );
    }

    public function detail(string $number): ?Responses\ResponseDocumentDetail
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::DOCUMENTS_ORDER_INFO, ['clientNumber' => $number]),
            Responses\ResponseDocumentDetail::class
        );
    }

    public function getRelatedDocument(string $number): ?Responses\ResponseRelatedDocuments
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::DOCUMENTS_RELATED_ORDERS, ['number' => $number]),
            Responses\ResponseRelatedDocuments::class
        );
    }

    public function checkDocumentChange(string $number): ?Responses\ResponseCheckDocumentChange
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::DOCUMENTS_ORDERS_CHECK, ['number' => $number]),
            Responses\ResponseCheckDocumentChange::class
        );
    }

    public function documentChange(
        string $number,
        Requests\DocumentChangeRequest $request
    ): ?Responses\ResponseDocumentChange {
        return self::factoryEntity(
            self::doPackDataRequest(HttpClient::POST, self::DOCUMENTS_ORDERS_CHANGE, $request, ['number' => $number]),
            Responses\ResponseDocumentChange::class
        );
    }

    public function checkReturnDocument(string $number): ?Responses\ResponseCheckReturnDocument
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::DOCUMENTS_RETURN_ORDERS_CHECK, ['number' => $number]),
            Responses\ResponseCheckReturnDocument::class
        );
    }

    public function returnDocument(string $number): ?Responses\ResponseReturnDocument
    {
        return self::factoryEntity(
            self::doRequest(HttpClient::POST, self::DOCUMENTS_CREATE_RETURN, ['number' => $number]),
            Responses\ResponseReturnDocument::class
        );
    }

    public function checkRedirectDocument(
        string $number,
        Requests\RedirectDocumentRequest $request = null
    ): ?Responses\ResponseCheckRedirectDocument {
        if ($request) {
            $data = $request->getData();
        }

        return self::factoryEntity(
            self::doRequest(
                HttpClient::POST,
                self::DOCUMENTS_REDIRECTING_CHECK,
                array_merge(['number' => $number], $data ?? [])
            ),
            Responses\ResponseCheckRedirectDocument::class
        );
    }

    public function redirectDocument(
        string $number,
        Requests\RedirectDocumentRequest $request
    ): ?Responses\ResponseRedirectDocument {
        $data = $request->getData();

        return self::factoryEntity(
            self::doRequest(
                HttpClient::POST,
                self::DOCUMENTS_REDIRECTING_CREATE,
                array_merge(['number' => $number], $data)
            ),
            Responses\ResponseRedirectDocument::class
        );
    }

    public function setAgent(string $number, Requests\SetAgentRequest $request = null): ?Responses\ResponseSetAgent
    {
        if ($request) {
            $data = $request->getData();
        }

        return self::factoryEntity(
            self::doRequest(
                HttpClient::POST,
                self::DOCUMENTS_SET_AGENT_IN_ORDER,
                array_merge(['number' => $number], $data ?? [])
            ),
            Responses\ResponseSetAgent::class
        );
    }

    public function calculate(Requests\CalculateRequest $request): ?Responses\ResponseCalculate
    {
        return self::factoryEntity(
            self::doPackDataRequest(HttpClient::POST, self::DOCUMENTS_CALCULATE_PRICE, $request),
            Responses\ResponseCalculate::class
        );
    }

    protected function useType(): int
    {
        return HttpClient::USE_V1;
    }
}
