## Status
       
[![Latest Stable Version](https://poser.pugx.org/fbkl/flax/v/stable)](https://packagist.org/packages/fbkl/flax)
[![pipeline status](https://gitlab.com/fabrika-klientov/libraries/flax/badges/master/pipeline.svg)](https://gitlab.com/fabrika-klientov/libraries/flax/commits/master)
[![License](https://poser.pugx.org/fbkl/flax/license)](https://packagist.org/packages/fbkl/flax)


**Клиент для работы с API Justin**

[Документация](https://justin.ua/api/api_justin_documentation.pdf)

---

## Usage

`composer require fbkl/flax`

---

## Работа с библиотекой

### Client (Клиент для работы с Justin)

Для начала работы с библиотекой УП необходимо создать инстанс клиента, передав ему 
необходимые данные для авторизации

1. Создайте объект `Credentials`:
```injectablephp
use Flax\Core\Auth\Credentials;

// Все параметры опциональны
$credentials = new Credentials('Just', 'Just', 'f2290c07-c028-11e9-80d2-525400fb7782');

// или
$credentials = new Credentials();
$credentials->setLogin('Just');
$credentials->setPassword('Just');
$credentials->setApikey('f2290c07-c028-11e9-80d2-525400fb7782');

```

2. Создайте клиент `Client`
(если передать второй параметр `true` - Клиент будет использоваться
в тест режиме):
```injectablephp
use Flax\Client;

$client = new Client($credentials);
$clientTest = new Client($credentials, true);

```

**Некоторые методы используют `accessToken` (oauth) для запросов.
По умолчанию данные авторизации хранятся в файлах (с названиями логинов клиентов)
в директории `storage/auth/flax/` (с помощью `AuthFileJsonAdapter`).
Есть возможность изменить поведение передав свой адаптер реализовав интерфейс
`BeAuthAdapter`.**

```injectablephp
use Flax\Client;

class MyAdapter implements \Flax\Contracts\BeAuthAdapter
{
    public function read(string $login): array
    {
        // todo ....
        return ['accessToken' => '...', 'refreshToken' => '...'];
    }
    
    public function write(string $login, array $data): void
    {
        // todo save
    }
}

$client = new Client($credentials);
$client->withAuthAdapter(new MyAdapter());
```


3. С помощью клиента вы можете создать (вызвать) нужный сервис для работы:
```injectablephp
$service1 = $client->getAddressesService();
$service2 = $client->getDocumentsService();
$service3 = $client->getStickersService();
// ...
```

### Services

Доступные сервисы:
- `AddressesCityService` - Работа с адресами (города, улицы, дома) `v3`
- `AddressesService` - Работа с адресами
- `DocumentsService` - Работа с накладными (документами) 
- `RegistersService` - Работа с регистрами
- `StatusesService` - Работа с статусами накладных
- `StickersService` - Работа с стикерами
- `AdditionalService` - Работа с дополнительными данными

---

Во время работы с сервисами - все запросы строятся с помощью
объекта конструктора запросов на вход необходимого метода сервиса
(если эти данные нужны методу).

Результат выполнения будет всегда объект (сущность) (Response со статусами и данными).
Внутри Response данные определенного класса или коллекция этих объектов (сущностей),
за исключением `StickersService` - в результате может быть
строка ссылки на стикер или бинарные данные формы (строка).

`TODO: По надобности расписать работу по каждому сервису и его методах отдельно`

### Service AddressesService

...

### Service AddressesCityService

...

---

### Create Document

```injectablephp
use Flax\Entities\Documents\Requests\CreateDocumentRequest;

// Создаем запрос на создание накладной
// с возможностью передать данные через конструктор объекта
$request = new CreateDocumentRequest([
    "number" => "20211026555",
    "date" => "20211026",
    "sender_city_id" => "50a09bef-dc05-11e7-80c6-00155dfbfb00",
    "sender_company" => "TESTAPI",
    "sender_contact" => "Представитель",
    "sender_phone" => "+380503386443",
    "sender_pick_up_address" => "01030, Київ, вул. Б.Хмельницького, 44",
    "pick_up_is_required" => true,
    "sender_branch" => "58748012",
    "receiver" => "Петров Сергей",
    "receiver_contact" => "",
    "receiver_phone" => "+380978728877",
    "count_cargo_places" => 2,
    "branch" => "7100104224",
    "volume" => 0.02,
    "weight" => 2.5,
    "delivery_type" => 0,
    "declared_cost" => 1500,
    "delivery_amount" => 0,
    "redelivery_amount" => 1500,
    "order_amount" => 1500,
    "redelivery_payment_is_required" => true,
    "redelivery_payment_payer" => 1,
    "delivery_payment_is_required" => true,
    "delivery_payment_payer" => 1,
    "order_payment_is_required" => true
]);
// или передавать по цепочке запросов (возможно удобнее)
$request
    ->date('20211026')
    ->senderCityId('50a09bef-dc05-11e7-80c6-00155dfbfb00')
    ->senderPhone('+380503386443');

try {
    // Создание накладной
    /**
     * @var \Flax\Entities\Documents\Responses\ResponseStoreDocument $response
     * */
    $response = $client->getDocumentsService()->create($request);
} catch (\Flax\Exceptions\ResponseErrorException $exception) {
    // если данные не прошли валидацию на серверах Justin
}

// массив данных новой накладной
$arrayData = $response->data;
$number = $arrayData['number'];

// или сущность (объект)
/**
 * @var \Flax\Entities\Documents\Document $objectData
 * */
$objectData = $response->data();
$number = $objectData->number;
```


