<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Addresses;

use Flax\Contracts\BeEntity;
use Flax\Entities\Addresses\Additional\AreaSimple;
use Flax\Entities\Addresses\Additional\RegionSimple;

/**
 * @property-read string $uuid
 * @property-read string $code
 * @property-read string $descr
 * @property-read string $SCOATOU
 * @property-read array $objectOwner
 * @property-read array $district
 * */
class City extends FieldsEntity implements BeEntity
{
    public function objectOwner(): AreaSimple
    {
        return new AreaSimple($this->objectOwner);
    }

    public function district(): RegionSimple
    {
        return new RegionSimple($this->district);
    }
}
