<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Flax
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.26
 * @link      https://fabrika-klientov.ua
 */

namespace Flax\Entities\Base\V1;

use Flax\Entities\Entity;

/**
 * @property-read string $error
 * */
class ResponseError extends Entity
{
    public function __construct($data = [])
    {
        if (!is_array($data)) {
            $data = ['error' => $data];
        }

        parent::__construct($data);
    }
}
